import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';

/**
 * Generated class for the DetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-details',
  templateUrl: 'details.html',
})
export class DetailsPage {
  

  item: any;
  temp:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private http: HttpClient) {
    this.item = navParams.get('item');
    this.http.get('http://api.openweathermap.org/data/2.5/weather?q='+this.item.text+'&appid=61749a6424601b2505416be079fd13dd').subscribe(data => {
    //  alert(JSON.stringify(data['main']['temp']));
    this.temp = ( (parseFloat(data['main']['temp'])* 9/5-459.67).toFixed(2));
    });

  }


  ngOnInit(): void {
   
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailsPage');
  }

}
