import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DetailsPage } from '../details/details';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  items: any[];
  constructor(public navCtrl: NavController) {
    this.items = [];

    this.items.push({ text: "Chennai", id: 1 });
    this.items.push({ text: "Mclean", id: 1 });
    this.items.push({ text: "Cincinnati", id: 1 });
    this.items.push({ text: "London", id: 1 });
  }
    itemSelected(item){
      this.navCtrl.push(DetailsPage, {
        item: item
      })
    }
  }
